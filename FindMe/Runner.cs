﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finder;

namespace FindMe
{
    class Runner
    {
        static Dictionary<char, string> options = new Dictionary<char, string>()
        {
            { 'h', "Show all available options and help" },
            { 'v', "Verbose mode, show all console output" },
            { 'd', "Do not email results"},
            { 'w', "wait mode, waits for user input" }
        };

        public static Find instance;
        static bool verbose = false;
        static bool email = true;
        static bool wait = false;
        static int exitCode = 0;

        static int Main(string[] args)
        {
            Console.WriteLine("Querying for information:");
            instance = new Find();

            if (args.Length == 2)
            {
                instance._config.Email = args[0].ToString();
                instance._config.Password = args[1].ToString();
            }
            else if (args.Length == 3)
            {
                List<char> arguments = new List<char>();

                foreach (char a in args[0].ToCharArray())
                {
                    if (a != '-')
                    {
                        if (options.ContainsKey(char.ToLower(a)))
                            arguments.Add(char.ToLower(a));
                        else
                        {
                            Console.WriteLine("Invalid option '-" + a + "'");
                            printOptions();
                            return exitCode = -1;
                        }
                    }
                }
                parseOptions(arguments);
                instance._config.Email = args[1].ToString();
                instance._config.Password = args[2].ToString();
            }
            else
            {
                Console.WriteLine("Useage: findme.exe [options] [email] [password]");
                printOptions();
                return exitCode = -1;
            }

            printConfig();

            if (checkStats())
            {
                if (email)
                {
                    consoleWrite("Sending email...");
                    instance.emailResults();
                }
            }
            
            consoleWrite("Exiting with value: " + exitCode);
            if (wait)
            {
                consoleWrite("Press any key to exit");
                Console.ReadKey();
            }
            return exitCode;
        }

        private static void printOptions()
        {
            Console.WriteLine("Options:");
            foreach (var opt in options)
            {
                Console.Write("-" + opt.Key);
                Console.WriteLine(" " + opt.Value);
            }
            debug();
        }

        private static void consoleWrite(string s)
        {
            if (verbose)
                Console.WriteLine(s);
        }

        private static void parseOptions(List<char> a)
        {
            foreach (char c in a)
            {
                if (c == 'v')
                    verbose = true;
                if (c == 'd')
                    email = false;
                if (c == 'w')
                    wait = true;
                if (c == 'h' && a.Count >= 1)
                    Console.WriteLine("Ignoring help option");
                else if (c == 'h' && a.Count == 0)
                    printOptions();
            }            
        }

        private static bool checkStats()
        {
            consoleWrite("Checking if External IP has changed.");
            
            if (instance._config.ConfigLoaded == false)
            {
                consoleWrite("First run, updating IP");
                return true;
            }

            Config temp = instance.getInformation();
            if (instance.changed(instance._config, temp))
            {
                consoleWrite("External IP address has changed");
                instance._config.ExternalIP = temp.ExternalIP;
                instance._config.InternalIP = temp.InternalIP;
                instance._config.ComputerName = temp.ComputerName;
                instance._config.CurrentUser = temp.CurrentUser;
                instance._config.LastUpdateTime = temp.LastUpdateTime;
                instance._config.SuccessfulUpdateTimes.Add(temp.LastUpdateTime);
                return true;
            }
            consoleWrite("External IP address has not changed");
            return false;
        }

        private static void printConfig()
        {
            consoleWrite("Computer Name : " + instance._config.ComputerName);
            consoleWrite("Current  User : " + instance._config.CurrentUser);
            consoleWrite("External IP   : " + instance._config.ExternalIP);
            consoleWrite("Internal IP   : " + instance._config.InternalIP);
            consoleWrite("Email Address : " + instance._config.Email);
            consoleWrite("Update History: ");
            foreach (var i in instance._config.SuccessfulUpdateTimes)
                consoleWrite(i.ToString() + "\n");
        }

        protected static void debug()
        {
            #if DEBUG
                Console.ReadKey();
            #endif
        }
    }
}
