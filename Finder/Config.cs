﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Finder
{
    [Serializable]
    public class Config
    {
        private static Config _instance;

        public string InternalIP { get; set; }
        public string ExternalIP { get; set; }
        public string ComputerName { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public List<DateTime> SuccessfulUpdateTimes { get; set; }
        public string CurrentUser { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool ConfigLoaded { get; set; }
        
        public const string _location = @"config.dat";

        public Config()
        {
            SuccessfulUpdateTimes = new List<DateTime>();
        }

        public bool Load()
        {
            if (!File.Exists(_location))
                return false;

            try
            {
                IFormatter formatter = new BinaryFormatter();
                using (Stream stream = new FileStream("config.dat", FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    Config temp = (Config)formatter.Deserialize(stream);
                    InternalIP = temp.InternalIP;
                    ExternalIP = temp.ExternalIP;
                    ComputerName = temp.ComputerName;
                    LastUpdateTime = temp.LastUpdateTime;
                    SuccessfulUpdateTimes = temp.SuccessfulUpdateTimes;
                    CurrentUser = temp.CurrentUser;
                    Email = temp.Email;
                    Password = temp.Password;
                    ConfigLoaded = true;
                }
                return true;
            } catch (Exception)
            {
                ConfigLoaded = false;
                return false;
            }
        }

        public void Save()
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new FileStream("config.dat", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                formatter.Serialize(stream, this);
            }
        }
    }
}
