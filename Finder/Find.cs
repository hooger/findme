﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Net.Mail;

namespace Finder
{
    public class Find
    {
        public Config _config;

        private const string MailServerString = "smtp.gmail.com";

        public Find()
        {
            _config = new Config();
            if (!_config.Load())
            {
                _config = getInformation();
                _config.Save();
            }
        }

        public Config getInformation()
        {
            Config tempConfig = new Config();

            //Get IP information
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                tempConfig.InternalIP = getInteralIP();
                tempConfig.ExternalIP = getExternalIP();
                tempConfig.SuccessfulUpdateTimes.Add(DateTime.Now);
            }
            else
            {
                tempConfig.InternalIP = "Unavailable";
                tempConfig.ExternalIP = "Unavailable";
            }

            //Get other information
            tempConfig.LastUpdateTime = DateTime.Now;
            tempConfig.ComputerName = System.Environment.MachineName;
            tempConfig.CurrentUser = System.Environment.UserName;

            return tempConfig;
        }

        public bool changed(Config older, Config newer)
        {
            if (older.ExternalIP != newer.ExternalIP || older.ConfigLoaded == false)
                return true;
            return false;
        }

        public void Save()
        {
            _config.Save();
        }

        public bool emailResults()
        {
            if (string.IsNullOrWhiteSpace(_config.Email) || string.IsNullOrWhiteSpace(_config.Password))
                return false;

            try
            {
                var fromAddress = new MailAddress(_config.Email, "IP Checker");
                var toAddress = new MailAddress(_config.Email);
                string subject = "New IP Address for: " + _config.ComputerName;
                var s = new StringBuilder();
                s.AppendLine("The computer " + _config.ComputerName + " has changed it's external IP");
                s.AppendLine("Here is the new information:");
                s.AppendLine("Computer: " + _config.ComputerName);
                s.AppendLine("User: " + _config.CurrentUser);
                s.AppendLine("Internal IP: " + _config.InternalIP);
                s.AppendLine("External IP: " + _config.ExternalIP);
                s.AppendLine("Updated At: " + _config.LastUpdateTime);
                s.AppendLine("Update History:");
                foreach (var time in _config.SuccessfulUpdateTimes)
                    s.AppendLine(time.ToString());

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, _config.Password)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = s.ToString()
                })
                {
                    smtp.Send(message);
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        private string getExternalIP()
        {
            var request = (HttpWebRequest)WebRequest.Create("http://ifconfig.me");
            request.UserAgent = "curl";
            request.Method = "GET";

            string externalIP = "";
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        externalIP = reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (!string.IsNullOrWhiteSpace(externalIP))
                return externalIP.Replace("\n", "");
            return "Unavailable";
        }

        private string getInteralIP()
        {            
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;            
        }
    }
}
